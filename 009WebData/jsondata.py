import urllib.request
import json

def printResults(data):
	the_json = json.loads(data)
	if "title" in the_json["metadata"]:
		print(the_json["metadata"]["title"])
	count = the_json["metadata"]["count"]
	print(str(count) + "events recorded")
	#For each event, print the place where it ocurred
	for i in the_json["features"]:
		print(i["properties"]["place"])
	print("---------------------\n")
    
	#print the events that only have a magnitude greater than 4
	for i in the_json["features"]:
		if i["properties"]["mag"] >= 0.6:
			print("%2.1f " % i["properties"]["mag"], i["properties"]["place"])
	print("---------------------\n")
	
	#print the events that sources are ,nn,
	for i in the_json["features"]:
		if i["properties"]["sources"] != None:
			#print("hw")
			if i["properties"]["sources"] == ",nn,":
				print("%2.1f" %i["properties"]["mag"], i["properties"]["place"], i["properties"]["sources"])
	print("---------------------\n")
	

def main():
	urlData = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson"
	webUrl = urllib.request.urlopen(urlData)
	#print("result code: "+ str(webUrl.getcode()))
	data = webUrl.read()
	#print(data)
	printResults(data)


if __name__ == "__main__":
	main()
