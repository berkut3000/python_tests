
def main():
	x, y = 100, 1000

	if (x < y):
		st = "x is less than y"
	elif(x == y):
		st = "x equals y"
	else:
		st = "x is more than y"
	print(st)
	
	st = "x is less than y" if (x<y) else "x is greater than or the same as y"
	print(st)
if __name__ == "__main__":
	main()
