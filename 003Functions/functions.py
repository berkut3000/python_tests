def func1():
	print("I'm a function")

def func2(arg1, arg2):
	print(arg1, " ", arg2)

def func3(x):
	return x*x*x

def func4(num, x= 1):
	result = 1
	for i in range(x):
		result = result * num
	return result

def func5(*args):
	result = 0
	for x in args:
		result = result + x
	return result

def func6(x = "cadena", *args):
	print(x)
	result = 0
	for x in args:
		result = result + x
	return result

func1()
print(func1())
print(func1)

func2("hola","adios")

print(func3(5))
print(func4(6))
print(func4(6,3))
print(func4(x = 4, num = 4))
print(func5(3,4))
print(func5(3,4,10))
print(func5(3,4,6,8))
print(func5(3,4,4,5))
print(func5(3,4,4,5,6,5,4,3,3,3,3,3))
print(func6())
print(func6("Esta es una prueba"))
print(func6("hola2", 4,3,3,3,3,3,3,3,3))
