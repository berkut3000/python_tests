#Rojo, Verde y Azul
import pygame,sys
from pygame.locals import *
from random import randint

pygame.init()
ventana = pygame.display.set_mode((600,600))
pygame.display.set_caption("Tutorial 8")

Mi_imagen = pygame.image.load("imagenes/7.png")
posx = 10
posy = 10

ventana.blit(Mi_imagen,(posx,posy))
velocidad=8
blanco=(255,255,255)
derecha=True
while True:
	ventana.fill(blanco)
	ventana.blit(Mi_imagen,(posx,posy))
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key==K_LEFT:
				posx-=velocidad
			elif event.key==K_RIGHT:
				posx+=velocidad
	pygame.display.update()
