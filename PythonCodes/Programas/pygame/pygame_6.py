#Rojo, Verde y Azul
import pygame,sys
from pygame.locals import *
from random import randint

pygame.init()
ventana = pygame.display.set_mode((600,600))
pygame.display.set_caption("Tutorial 7")

Mi_imagen = pygame.image.load("imagenes/7.png")
posx = randint(10,300)
posy = randint(10,300)

ventana.blit(Mi_imagen,(posx,posy))

while True:
	for evento in pygame.event.get():
		if evento.type == QUIT:
			pygame.quit()
			sys.exit()
	pygame.display.update()
