#Rojo, Verde y Azul
import pygame,sys
from pygame.locals import *

pygame.init()
ventana = pygame.display.set_mode((400,300))
pygame.display.set_caption("Tutorial 4")

color2 = pygame.Color(130,180,150)
pygame.draw.line(ventana,color2,(60,180),(160,100),8)

print color2.r
print color2.g
print color2.b

while True:
	for evento in pygame.event.get():
		if evento.type == QUIT:
			pygame.quit()
			sys.exit()
	pygame.display.update()
