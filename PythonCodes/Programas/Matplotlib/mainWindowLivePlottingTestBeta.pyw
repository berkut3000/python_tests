#!/usr/bin/python
from Tkinter import *
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import time
import sys

ventana = Frame(height=480,width=800)
fig = plt.figure(figsize=(7.5,4))
fig, axes = plt.subplots(nrows=2)
styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']
x = np.arange(0, 2*np.pi, 0.1)
y = np.sin(x)

def winSetup():
        ventana.config(bg="blue")
        ventana.pack()

def figSetup():
        Figure = FigureCanvasTkAgg(fig, master=ventana)
        Figure.get_tk_widget().place(x=20, y=50)

def Salir():
        ventana.destroy()
        sys.exit(0)

def botSetup():
        bMuestreo=Button(ventana, text="Escanear", command=grafico).place(x=10,y=10)
        bSalir=Button(ventana, text="Salir", command=Salir).place(x=100,y=10)

def plot(ax, style):
            return ax.plot(x, y, style, animated=True)[0]

lines = [plot(ax, style) for ax, style in zip(axes, styles)]

def grafico():
        
##        Se debe dibujar el canvas primero antes de animarlo
        fig.canvas.draw()
##        Se captura el fondo
        backgrounds= [fig.canvas.copy_from_bbox(ax.bbox) for ax in axes]
        for i in xrange(1, 50):
            items = enumerate(zip(lines, axes, backgrounds), start=1)
            for j, (line, ax, background) in items:
                fig.canvas.restore_region(background)
                line.set_ydata(np.sin(j*x + i/10.0))
                ax.draw_artist(line)
                fig.canvas.blit(ax.bbox)

winSetup()

figSetup()

botSetup()

ventana.mainloop()
