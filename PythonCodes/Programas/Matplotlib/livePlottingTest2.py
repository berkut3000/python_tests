import matplotlib.pyplot as plt
import numpy as np
import time

x = np.arange(0, 2*np.pi, 0.1)
y = np.sin(x)

fig, axes = plt.subplots()

fig.show()

# We need to draw the canvas before we start animating...
fig.canvas.draw()

styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']
def plot(ax, style):
    return ax.plot(x, y, style, animated=True)[0]


# Let's capture the background of the figure
backgrounds = fig.canvas.copy_from_bbox(axes.bbox)
lines = plot(axes, styles[0])
tstart = time.time()
for i in xrange(1, 2000):
##    items = enumerate(zip(lines, axes, backgrounds), start=1)
##    for j, (line, ax, background) in items:
            fig.canvas.restore_region(backgrounds)
            lines.set_ydata(np.sin(x + i/10.0))
            axes.draw_artist(lines)
            fig.canvas.blit(axes.bbox)
            

print 'FPS:' , 2000/(time.time()-tstart)
