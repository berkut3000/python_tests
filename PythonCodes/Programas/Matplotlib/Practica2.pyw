#!/usr/bin/python
from Tkinter import *
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import time
import sys

def salir():
        ventana.destroy()
        sys.exit(0)

def grafico():
        x=np.arange(100)
        y=np.sin(x)
        plt.plot(x,y)
        fig.canvas.draw()

ventana = Frame(height=480,width=800)
ventana.config(bg="gray")
ventana.pack()

fig = plt.figure(figsize=(7.5,4))
Figure = FigureCanvasTkAgg(fig, master=ventana)
Figure.get_tk_widget().place(x=20, y=50)

bMuestreo=Button(ventana, text="Escanear", command=grafico)
bMuestreo.place(x=10,y=10)

bSalir=Button(ventana, text="Salir", command=salir)
bSalir.place(x=100,y=10)

ventana.mainloop()
