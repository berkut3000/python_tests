#!/usr/bin/python
import RPi.GPIO as GPIO
import time
## pines = [02,03,04,14,15,18,17]
pines = [02,03,04,17,27,22,10,9]
sum = 0
l = len(pines)
ran = range(0,l)
GPIO.setmode(GPIO.BCM)
## pinMuestreo = 18;
## pinDatoRecibido = 23;
## pinTransmisionConcluida = 24;
vec = []
GPIO.setup(02,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(03,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(04,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(10,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(9,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18,GPIO.OUT)#Iniciar Muestreo
GPIO.setup(23,GPIO.IN, pull_up_down=GPIO.PUD_UP)#DatoRecibido
GPIO.setup(24,GPIO.IN, pull_up_down=GPIO.PUD_UP)#TransmisionConluida

def pH(pin):
	GPIO.output(pin,1)
	
def pL(pin):
	GPIO.output(pin,0)

def lectura():
	s = 0
	global sum
	for i in ran:
		estEnt = GPIO.input(pines[i])
		if estEnt == True:
			print "Pin %d leido" %i
			s = s+2**i
		else:
			print "Pin %d no leido" %i
			s = s
	sum =  s
	return sum
##	time.sleep(0.1)

def iM(imc):		##Iniciar muestreo
	if imc == 1:
		pH(18)
	else:
		pL(18)
	

def inicio():
	start = input("Escriba 1 para iniciar muestreo")
	if start == 1:
		iM(1)
		res =lectura()
		print res
##		iM(1)
##		estTran = GPIO.input(24)
##		while(estTran == 0):
##			estDat = GPIO.input(23)
##			if(estDat == 1):
##				temp = lectura()
##				vec.append(temp)
##			else:
##						
##			estTran = GPIO.input(24)
	else:
		print "Comando no valido, vuelva a intentarlo"
		iM(0)

while True:
	inicio()
##	for i in ran:
##		estEnt = GPIO.input(pines[i])
##		if estEnt == True:
##			print "Pin %d leido" %i
##		else:
##			print "Pin %d no leido" %i
##	time.sleep(0.1)
