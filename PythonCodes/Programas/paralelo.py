#!/usr/bin/python
import RPi.GPIO as GPIO
import time
## pines = [02,03,04,14,15,18,17]
pines = [02,03,04,14,15,18,17,27,22,23,24]
sum = 0
l = len(pines)
ran = range(0,l)
GPIO.setmode(GPIO.BCM)

GPIO.setup(02,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(03,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(04,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(14,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(15,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23,GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24,GPIO.IN, pull_up_down=GPIO.PUD_UP)

def estado(pin,pot):
	s = 0
	estEnt = GPIO.input(pin)
	if estEnt == False:
		s = 2**pot
	global sum
	sum = sum + s
	return sum

def lectura():
	for i in ran:
		estEnt = GPIO.input(pines[i])
		if estEnt == True:
			print "Pin %d leido" %i
		else:
			print "Pin %d no leido" %i
	time.sleep(0.1)


while True:
	lectura()
##	for i in ran:
##		estEnt = GPIO.input(pines[i])
##		if estEnt == True:
##			print "Pin %d leido" %i
##		else:
##			print "Pin %d no leido" %i
##	time.sleep(0.1)
