#!/usr/bin/python
from Tkinter import *
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
##import time

ventana = Frame(height=480,width=800)
fig = plt.figure()
fig, axes = plt.subplots(figsize=(7.5,4))
styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']

x = np.arange(0, 1024, 100)
y = np.sin(x)

def winSetup():
        ventana.config(bg="black")
        ventana.pack()

def figSetup():
        Figure = FigureCanvasTkAgg(fig, master=ventana)
        Figure.get_tk_widget().place(x=20, y=50)

def Salir():
        ventana.destroy()
        sys.exit(0)

def botSetup():
        bMuestreo=Button(ventana, text="Escanear", command=grafico).place(x=10,y=10)
        bSalir=Button(ventana, text="Salir", command=Salir).place(x=100,y=10)

def plot(ax, style):
            return ax.plot(x, y, style, animated=True)[0]



def grafico():
        axes.set_title("FFT")
        axes.set_autoscaley_on(False)
        axes.set_ylim([0,255])
        axes.set_xlim([0,1024])
##        Se debe dibujar el canvas primero antes de animarlo
        fig.canvas.draw()
##        Se captura el fondo
        backgrounds = fig.canvas.copy_from_bbox(axes.bbox)
        lines = plot(axes, styles[0])
        for i in xrange(1, 50):
            fig.canvas.restore_region(backgrounds)
            lines.set_ydata((255*np.sin(x + i/10.0))/2 + 128)
            axes.draw_artist(lines)
            fig.canvas.blit(axes.bbox)

winSetup()

figSetup()

botSetup()

ventana.mainloop()

