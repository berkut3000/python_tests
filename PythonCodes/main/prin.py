#!/usr/bin/python
#Programa principal
from Tkinter import *
import numpy as np
import matplotlib
import rModule
##from oModule import abrir as ab
from rModule import leer as rd
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

dl = 1025       #Longitud de cadena de datos
ventana = Frame(height=480,width=800)
fig = plt.figure()
fig, axes = plt.subplots(figsize=(7.5,4))
styles = ['r-', 'g-', 'y-', 'm-', 'k-', 'c-']

x = np.arange(0, dl)
y = x

def winSetup():
        ventana.config(bg="black")
        ventana.pack()

def figSetup():
        Figure = FigureCanvasTkAgg(fig, master=ventana)
        Figure.get_tk_widget().place(x=20, y=50)

def Salir():
        ventana.destroy()
        sys.exit(0)

def botSetup():
        bMuestreo=Button(ventana, text="Escanear", command=grafico).place(x=10,y=10)
        bSalir=Button(ventana, text="Salir", command=Salir).place(x=100,y=10)

def plot(ax, style):
            return ax.plot(x, y, style, animated=True)[0]


def grafico():
        y= rd()
        axes.set_title("Seal Ultra")
        axes.set_autoscaley_on(False)
        axes.set_ylim([0,255])
        axes.set_xlim([0,dl])
##        Se debe dibujar el canvas primero antes de animarlo
        fig.canvas.draw()
##        Se captura el fondo
        backgrounds = fig.canvas.copy_from_bbox(axes.bbox)
        lines = plot(axes, styles[0])
        while(1):
                y= rd()
                fig.canvas.restore_region(backgrounds)
                lines.set_ydata(y)
                axes.draw_artist(lines)
                fig.canvas.blit(axes.bbox)
##                y= rd()
##                y=ab()
##                len_y = len(y)
##                x=np.arange(len_y)
##                plt.plot(x,y)
                
##                
##                fig.canvas.draw()
winSetup()

figSetup()

botSetup()

ventana.mainloop()
