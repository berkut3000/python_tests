import serial # import Serial Library
import numpy  # Import numpy
import matplotlib.pyplot as plt #import matplotlib library
from drawnow import *
import time
import struct
i=0
vec = []
port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=0.02) #Creating our serial object named arduinoData
plt.ion() #Tell matplotlib you want interactive mode to plot live data
cnt=0

def makeFig(): #Create a function that makes our desired plot
    plt.ylim(-150,150)                                 #Set y min and max values
    plt.title('My Live Streaming Sensor Data')      #Plot the title
    plt.grid(True)                                  #Turn the grid on
    plt.ylabel('Temp F')                            #Set ylabels
    plt.plot(vec, 'ro-', label='Degrees F')       #plot the temperature
    plt.legend(loc='upper left')                    #plot the legend
        

while True: # While loop that loops forever
    while (port.inWaiting()==0): #Wait here until there is data
        pass #do nothing
    rcv = port.read(1) #read the line of text from the serial port
    dato = struct.unpack("b",rcv)   #Split it into an array called dataArray
    vec.append(dato[0])            #Convert first element to floating number and put in temp
    drawnow(makeFig,show_once=True)                       #Call drawnow to update our live graph
##    plt.pause(.000001)                     #Pause Briefly. Important to keep drawnow from crashing
##    cnt=cnt+1
##    if(cnt>50):                            #If you have 50 or more points, delete the first one from the array
##        vec.pop(0)                       #This allows us to just see the last 50 data points
##        vec.pop(0)
