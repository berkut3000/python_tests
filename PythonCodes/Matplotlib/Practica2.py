
from Tkinter import *
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

def grafico(): 
	x=np.arange(100)
	y=x**2
	plt.plot(x,y)
	fig.canvas.draw()

ventana = Frame(height=700,width=900)
ventana.config(bg="gray")
ventana.pack()

fig = plt.figure(figsize=(5,3))
Figure = FigureCanvasTkAgg(fig, master=ventana)
Figure.get_tk_widget().place(x=200, y=80)

boton=Button(ventana, text="boton", command=grafico)
boton.place(x=10,y=10)

ventana.mainloop()
