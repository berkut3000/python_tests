##Coordenadas
class Coordenadas:
    def __init__(self,x = 0,y = 0):
        self.x = x
        self.y = y
    def distancia(self,otro):
        dx = self.x-otro.x
        dy = self.y-otro.y
        return ((dx*dx+dy*dy)**0.5)
    def __del__(self):
        pass
   

#############Programa principal
c1 = Coordenadas(1,2)
c2 = Coordenadas(2,3)
print(c2.distancia(c1))
