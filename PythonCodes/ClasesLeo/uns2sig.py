##unsigned to signed
size = 8
value = 129
unsigned = value % 2**size 
signed = unsigned - 2**size if unsigned >= 2**(size-1) else unsigned
print(signed)
