#!/usr/bin/python
# -*- coding: cp1252 -*-
from Tkinter import *#Librería gráfica
import Tkinter as Tk
from PIL import ImageTk, Image#Importar libreria de imágenes

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import time
import sys

bA=0#Bandera subventana
##buttonWidth=10#Altura del boton
windowHeight=480#Altura ventana
windowWidth=800#Anchura Ventana
universalGeometry ='%dx%d' % (windowWidth,windowHeight)#Delimitador ventana
########################################################################
class aboutFrame(Tk.Toplevel):#Clase ventana de información
    """"""
 
    #----------------------------------------------------------------------
    def __init__(self, original):
        """Constructor"""
        global bA
        bA=1#Bnadera
        self.original_frame = original
        Tk.Toplevel.__init__(self)
##        self.geometry("400x240")
        self.title("Acerca de")
        self.abWidgets()

    def abWidgets(self):#Contenido de la ventana
        texto="Este programa fue desarrollado con fines academicos.\r\nQueda prohibido su uso para fines criminales.\r\nAutores:\r\nDr. JOrge Alberto Soto Cajiga\r\nMtro. Luis Alberto Carmona\r\nSu Alteza Serenisima Antonio de Jesus Aguilar Mota"
        l1 = Label(self,text=texto)
##        img = ImageTk.PhotoImage(Image.open("cidesi.png"))
##        panel = Label(root, image = img)
##        panel.pack(x=200,y=300)
        btn = Tk.Button(self, text="Cerrar", command=self.onClose)
        btn.grid(row=1,column=0)#Posicionamiento de objetos
        l1.grid(row=0,column=0)
 
    #----------------------------------------------------------------------
    def onClose(self): #Funcipón para cerrar la ventana.
        """"""
        global bA
        bA=0
        self.destroy()
        self.original_frame.show()
########################################################################
class confFrame(Tk.Toplevel):#Clase ventana configuración
    """"""
 
    #----------------------------------------------------------------------
    def __init__(self, original):
        """Constructor"""
        self.original_frame = original
        Tk.Toplevel.__init__(self,height=windowHeight,width=windowWidth)#Inicializar con parámetros de altura y anchura
        self.geometry(universalGeometry)#Delimitador de ventana
        rango_filas = range(0,20)#Confiugración de filas
        for i in rango_filas:
            self.rowconfigure(i,minsize=24)#Configuraación de columnas
        rango_columnas = range(0,10)
        for j in rango_columnas:
            self.columnconfigure(j,minsize=80)
        self.title("Configuraciones")
        self.confWidgets()

    def confWidgets(self):#Contenido de la ventana
        """Botones"""
        btnOp01 = Tk.Button(self, text="Opcion 1")
        btnOp02 = Tk.Button(self, text="Opcion 2")
        btnOp03 = Tk.Button(self, text="Opcion 3")
        btnOp04 = Tk.Button(self, text="Opcion 4")
        btnOp05 = Tk.Button(self, text="Opcion 5")
        btnOp06 = Tk.Button(self, text="Opcion 6")
        btnOp07 = Tk.Button(self, text="Opcion 7")
        btnOp08 = Tk.Button(self, text="Opcion 8")
        btnOp09 = Tk.Button(self, text="Opcion 9")
        btnOp10 = Tk.Button(self, text="Opcion 10")
        btnCerrar1 = Tk.Button(self, text="Cerrar", command=self.onClose)
##        btnCerrar2 = Tk.Button(self, text="Cerrar", command=self.onClose)
##        btnCerrar3 = Tk.Button(self, text="Cerrar", command=self.onClose)
##        btnCerrar4 = Tk.Button(self, text="Cerrar", command=self.onClose)
##        btnCerrar5 = Tk.Button(self, text="Cerrar", command=self.onClose)
        
        """Ubicación de botones"""
        btnOp01.grid(row=1,column=1,sticky=N+S+E+W)
        btnOp02.grid(row=4,column=1,sticky=N+S+E+W)
        btnOp03.grid(row=7,column=1,sticky=N+S+E+W)
        btnOp04.grid(row=10,column=1,sticky=N+S+E+W)
        btnOp05.grid(row=13,column=1,sticky=N+S+E+W)
        btnOp06.grid(row=1,column=3,sticky=N+S+E+W)
        btnOp07.grid(row=4,column=3,sticky=N+S+E+W)
        btnOp08.grid(row=7,column=3,sticky=N+S+E+W)
        btnOp09.grid(row=10,column=3,sticky=N+S+E+W)
        btnOp10.grid(row=13,column=3,sticky=N+S+E+W)
        btnCerrar1.grid(row=14,column=8,sticky=N+S+E+W)
##        btnCerrar2.grid(row=1,column=4,sticky=N+S+E+W)
##        btnCerrar3.grid(row=1,column=5,sticky=N+S+E+W)
##        btnCerrar4.grid(row=1,column=6,sticky=N+S+E+W)
##        btnCerrar5.grid(row=1,column=7,sticky=N+S+E+W)
        
        """Posicionar imagen"""
        #Imagen
        im=Image.open("cidesi.png")
        size = 320,271
        im.thumbnail(size)
        self.img = ImageTk.PhotoImage(im)
        panel = Label(self, image = self.img)
        panel.grid(row=3,column=5,columnspan=4,rowspan=5)

    #----------------------------------------------------------------------
    def onClose(self):#Función cerrar ventana
        """"""
        self.destroy()
        self.original_frame.show()
        
########################################################################
class scanFrame(Tk.Toplevel):#Clase ventana A-Scan
    """"""
 
    #----------------------------------------------------------------------
    def __init__(self, original):
        """Constructor"""
        self.original_frame = original
        Tk.Toplevel.__init__(self,height=windowHeight,width=windowWidth)#Inicializar con parámetros
        self.geometry(universalGeometry)#Delimitador de ventana
        rango_filas = range(0,20)#Configuración filas
        for i in rango_filas:
            self.rowconfigure(i,minsize=24)
        rango_columnas = range(0,11)#COnfiguración columnas
        for j in rango_columnas:
            self.columnconfigure(j,minsize=50)
        self.title("A-scan")
        self.scanWidgets()

    def scanWidgets(self):#Contenido de la ventana
        """Botones"""
        btnOp01 = Tk.Button(self, text="Opcion 1")
        btnOp02 = Tk.Button(self, text="Opcion 2")
        btnOp03 = Tk.Button(self, text="Opcion 3")
        btnOp04 = Tk.Button(self, text="Opcion 4")
        btnOp05 = Tk.Button(self, text="Opcion 5")
        btnOp06 = Tk.Button(self, text="Opcion 6")
        btnOp07 = Tk.Button(self, text="Opcion 7")
        btnOp08 = Tk.Button(self, text="Opcion 8")
        btnOp09 = Tk.Button(self, text="Opcion 9")
        btnOp10 = Tk.Button(self, text="Opcion 10")
        btn = Tk.Button(self, text="Cerrar", command=self.onClose)

        
        
        """Ubicación de botones"""
        btnOp01.grid(row=12,column=1,sticky=N+S+E+W)
        btnOp02.grid(row=12,column=3,sticky=N+S+E+W)
        btnOp03.grid(row=12,column=5,sticky=N+S+E+W)
        btnOp04.grid(row=12,column=7,sticky=N+S+E+W)
        btnOp05.grid(row=12,column=9,sticky=N+S+E+W)
        btnOp06.grid(row=13,column=1,sticky=N+S+E+W)
        btnOp07.grid(row=13,column=3,sticky=N+S+E+W)
        btnOp08.grid(row=13,column=5,sticky=N+S+E+W)
        btnOp09.grid(row=13,column=7,sticky=N+S+E+W)
        btnOp10.grid(row=13,column=9,sticky=N+S+E+W)
        btn.grid(row=15,column=9,sticky=N+S+E+W)

        #Imagen
        im=Image.open("cidesi.png")
        size = 320,271
        im.thumbnail(size)
        self.img = ImageTk.PhotoImage(im)
        panel = Label(self, image = self.img)
        panel.grid(row=3,column=3,columnspan=4,rowspan=5)
 
    #----------------------------------------------------------------------
    def onClose(self):
        """"""
        self.destroy()
        self.original_frame.show()

 
########################################################################
class MyApp(Frame):
 
    #----------------------------------------------------------------------
    def __init__(self, parent):
        """Constructor"""
        Frame.__init__(self, parent, height=windowHeight, width=windowWidth)#Inicializar con parámetros por defecto
        self.root = parent
        self.root.title("Bienvenido")
##        self.root.config(="black")
##        self.frame = Tk.Frame(parent)
##        self.frame.pack()
##        self.attributes("-toolwindow", 1)
        self.pack()
        self.create_widgets()

    def create_widgets(self):#Contenido de la ventana
        """Botones con características específicas"""
        strBtn = Button(self, text="A-scan", command=self.openFrame,height=3,width=30)
        stBtn = Button(self, text="Configuraciones", command=self.openFrame2,height=3,width=30)
        abBtn = Button(self, text="Acerca",command=self.openFrame3,height=3,width=30)
        exBtn = Button(self, text="Salir",command=self.Salir,height=3,width=30)
        strBtn.place(relx=0.01, rely=0.03)
        stBtn.place(relx=0.01, rely=0.29)
        abBtn.place(relx=0.01, rely=0.58)
        exBtn.place(relx=0.01, rely=0.85)

        #Imagen
        im=Image.open("cidesi.png")
        size = 375,271
        im.thumbnail(size)
        self.img = ImageTk.PhotoImage(im)
        panel = Label(self, image = self.img)
        panel.place(relx=0.45,rely=0.3)
        
    #----------------------------------------------------------------------
    def hide(self):#Función prara occultar ventana
        """"""
        self.root.withdraw()
 
    #----------------------------------------------------------------------
    def openFrame(self):#Función para abrir ventana de A-scan
        """"""
        self.hide()
        subFrame = scanFrame(self)

    #----------------------------------------------------------------------
    def openFrame2(self):#Función para abrir ventana de configuración
        """"""
        self.hide()
        subFrame = confFrame(self)
    #----------------------------------------------------------------------
    def openFrame3(self):#Función para abrir ventana de información
        """"""
##        state=
##        global bA
        subFrame = aboutFrame(self)
        state=subFrame.winfo_exists()
        print state
        if state==1:
          subFrame.destroy()
        subFrame = aboutFrame(self)
##        print subFrame.winfo_exists()
    #----------------------------------------------------------------------
    def show(self):#Función para mostrar la ventana
        """"""
        self.root.update()
        self.root.deiconify()
    #----------------------------------------------------------------------
    def Salir(self):#Función para salir del programa
        self.root.destroy()
        sys.exit(0)
 
#----------------------------------------------------------------------
if __name__ == "__main__":
    root = Tk.Tk()#Instanciaicón de las librerías gráficas
    root.geometry(universalGeometry)#Delimtador de ventana
    app = MyApp(root)#Instanciación de la ventana principal
    root.mainloop()#Ciclo infinito
