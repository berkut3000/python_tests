from datetime import date
from datetime import time
from datetime import datetime

def main():
	today = date.today()
	print("Today's date is ", today)
	print("Dates components: ",today.day, today.month, today.year)
	print("Today's weeknumber is ", today.weekday())
	
	days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
	print("Which is a:",days[today.weekday()])
	today = datetime.now()
	print(" The current date and time is ", today)

	t = datetime.time(datetime.now())
	s = datetime.date(datetime.now())
	print(t)
	print(s)

if __name__ == "__main__":
	main()
