from datetime import datetime


def main():
	now = datetime.now()
	print(now.strftime("The current year is: %Y"))
	print(now.strftime("%a, %d %b, %y"))

	print(now.strftime("Locale date and time: %c"))
	print(now.strftime("Locale date and time: %x"))
	print(now.strftime("Locale date and time: %X"))

	print(now.strftime("Current time: %I:%M:%Sa %p"))
	print(now.strftime("24-Hour time: %H:%M"))





if __name__ == "__main__":
	main()
